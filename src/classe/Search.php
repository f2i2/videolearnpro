<?php
namespace App\classe;


use App\classe\Category;

class Search {
/**
 * @var string
 */
public $string="";

/**
 * @var Category []
 */
public $categories = [];

public function getSearch() {
    return $this->string;
}

public function getcategories() {
    return $this->categories;
}

}
?>